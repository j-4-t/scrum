package com.jdriven.scrum;

public class ProductBacklogItem {

    private static final int DEFAULT_ESTIMATE = 1;

    private String summary;
    private int estimate;

    public ProductBacklogItem() {
        this.estimate = DEFAULT_ESTIMATE;
    }

    public ProductBacklogItem(String summary) {
        this();
        this.summary = summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getSummary() {
        return summary;
    }

    public void setEstimate(int estimate) throws IllegalEstimateException {
        if (estimate<0) {
            throw new IllegalEstimateException("Invalid value: " + estimate + ", estimate value should be 0 or more");
        }
        this.estimate = estimate;
    }

    public int getEstimate() {
        return estimate;
    }

    public String getShirtSize() {
        if (this.estimate < 5) {
            return "Small";
        } else if (this.estimate < 13) {
            return "Medium";
        } else {
            return "Large";
        }
    }
}
