package com.jdriven.scrum;

/**
 * Created by rb on 19/09/16.
 */
public class LimitedProductBacklog extends ProductBacklog {
    private int limit;


    public LimitedProductBacklog() {
        this(100);
    }


    public LimitedProductBacklog(int limit) {
        this.limit = limit;

    }

    public int getLimit() {
        return limit;
    }

    @Override
    public void addItem(ProductBacklogItem pbi) {
        int sum = this.getSumOfEstimates() + pbi.getEstimate();
        if (sum > this.limit) {
            throw new BacklogLimitReachedException("Could not add item, limit reached total score is: " + sum);
        }
        super.addItem(pbi);
    }
}
