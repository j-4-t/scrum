package com.jdriven.scrum;

public class UserStory extends ProductBacklogItem {

    private String acceptanceCriteria;

    public void setAcceptanceCriteria(String acceptanceCriteria) {
        this.acceptanceCriteria = acceptanceCriteria;
    }

    public String getAcceptanceCriteria() {
        return acceptanceCriteria;
    }
}
