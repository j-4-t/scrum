package com.jdriven.scrum;

import java.util.ArrayList;
import java.util.List;

public class ProductBacklog {
    private List<ProductBacklogItem> items = new ArrayList<>();

    public void addItem(ProductBacklogItem pbi) {
        items.add(pbi);
    }


    public ProductBacklogItem getItem(int index) {
        return items.get(index);
    }


    public int getNumberOfEstimates() {
        return items.size();
    }

    public int getSumOfEstimates() {
        int total = 0;

        for (ProductBacklogItem item : items) {
            total += item.getEstimate();
        }

        return total;
    }

    public ProductBacklogItem getHighestEstimate() {
        ProductBacklogItem highestEstimatedItem = null;

        for (ProductBacklogItem item : items) {
            if (highestEstimatedItem == null || item.getEstimate() > highestEstimatedItem.getEstimate()) {
                highestEstimatedItem = item;
            }
        }
        return highestEstimatedItem;
    }


    public void addItems(List<ProductBacklogItem> newItems) {
        items.addAll(newItems);
    }
}
