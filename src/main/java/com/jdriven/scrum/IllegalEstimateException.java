package com.jdriven.scrum;

/**
 * Created by rb on 05/09/16.
 */
public class IllegalEstimateException extends RuntimeException {
    public IllegalEstimateException() {
        super();
    }
    public IllegalEstimateException(String s) {
        super(s);
    }


}
