package com.jdriven.scrum;

public class BacklogLimitReachedException extends  RuntimeException {
    public BacklogLimitReachedException() {
        super();
    }
    public BacklogLimitReachedException(String s) {
        super(s);
    }

}
