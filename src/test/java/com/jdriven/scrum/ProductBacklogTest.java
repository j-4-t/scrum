package com.jdriven.scrum;


// In deze JUnit test gaan we stap voor stap een Scrum ProductBacklog met ProductBacklogItem objecten maken.
// Voor een beschrijving van wat een ProductBacklog is, zie
//   https://en.wikipedia.org/wiki/Scrum_(software_development)#Artifacts

// Maak de volledige implementatie door de REMOVE ME regel voor elke @Test methode te verwijderen (Ctrl + Y)
// de compileer fouten op te lossen, en de test te laten slagen.


import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ProductBacklogTest {

    
    //* -- REMOVE ME --

    // Maak een nieuwe class in de juiste package (com.jdriven.scrum), in src/main/java

    @Test
    public void productBacklogItemClass() {
        ProductBacklogItem pbi = new ProductBacklogItem();

        assertNotNull(pbi);
    }


    //* -- REMOVE ME --

    // Voeg het property summary toe

    @Test
    public void productBacklogItemFieldSummary() {
        ProductBacklogItem pbi = new ProductBacklogItem();
        pbi.setSummary("Login");

        assertEquals("Login", pbi.getSummary());
    }


    //* -- REMOVE ME --

    // Voeg het property estimate toe

    @Test
    public void productBacklogItemFieldEstimate() {
        ProductBacklogItem pbi = new ProductBacklogItem();
        pbi.setEstimate(7);

        assertEquals(7, pbi.getEstimate());
    }


    //* -- REMOVE ME --

    // Zet in de constructor de estimate waarde standaard op 1
    // Mogelijke verbetering : gebruik in ProductBacklogItem en constante DEFAULT_ESTIMATE om deze waarde te bepalen

    @Test
    public void productBacklogItemFieldEstimateHasDefaultOf1() {
        ProductBacklogItem pbi = new ProductBacklogItem();

        assertEquals(1, pbi.getEstimate());
    }


    //* -- REMOVE ME --

    // Maak een nieuwe constructor aan die de summary zet
    // Zorg ervoor dat alle voorgaande testen blijven werken!
    // Tip : De nieuwe constructor zal ook de reeds bestaande constructor aan moeten roepen

    @Test
    public void productBacklogItemConstructors() {
        ProductBacklogItem pbi = new ProductBacklogItem("Login");

        assertEquals("Login", pbi.getSummary());
        assertEquals(1, pbi.getEstimate());
    }


    //* -- REMOVE ME --

    // Maak een nieuwe method "getShirtSize()" aan die de "ShirtSize" van een item retourneert als String.
    // Volgens de onderstaande tabel:
    // < 5 Small
    // < 13 Medium
    // >= 13 Large

    // Tip : Gebruik een if -> else if -> else constructie

    @Test
    public void productBacklogItemShirtSize() {
        ProductBacklogItem pbiLogin = new ProductBacklogItem("Login");
        pbiLogin.setEstimate(3);

        assertEquals("Small", pbiLogin.getShirtSize());

        ProductBacklogItem pbiOverview = new ProductBacklogItem("Overview");
        pbiOverview.setEstimate(12);

        assertEquals("Medium", pbiOverview.getShirtSize());

        ProductBacklogItem pbiDetail = new ProductBacklogItem("Detail");
        pbiDetail.setEstimate(20);

        assertEquals("Large", pbiDetail.getShirtSize());
    }


    //* -- REMOVE ME --

    // Collections
    // Maak een nieuwe class ProductBacklog aan

    @Test
    public void productBacklogClass() {
        ProductBacklog productBacklog = new ProductBacklog();

        assertNotNull(productBacklog);
    }


    //* -- REMOVE ME --

    // Voeg een ArrayList toe aan de ProductBacklog
    // om de PBI's in op te slaan.

    // Maak nieuwe methoden om een PBI toe te voegen en op te halen.

    @Test
    public void productBacklogAddItemGetItem() {
        ProductBacklog productBacklog = new ProductBacklog();
        productBacklog.addItem(new ProductBacklogItem("Login"));
        productBacklog.addItem(new ProductBacklogItem("Overview"));

        assertEquals("Login", productBacklog.getItem(0).getSummary());
        assertEquals("Overview", productBacklog.getItem(1).getSummary());
    }


    //* -- REMOVE ME --

    // Maak nieuwe methoden om  meerdere PBI's toe te voegen en op te halen.

    @Test
    public void productBacklogAddMultipleItemGetItem() {
        ProductBacklog productBacklog = new ProductBacklog();

        List<ProductBacklogItem> items = new ArrayList<>();
        items.add(new ProductBacklogItem("Login"));
        items.add(new ProductBacklogItem("Overview"));
        productBacklog.addItems(items);

        assertEquals("Login", productBacklog.getItem(0).getSummary());
        assertEquals("Overview", productBacklog.getItem(1).getSummary());
    }


    //* -- REMOVE ME --

    // Maak een methode die het totaal estimates telt en retourneert

    @Test
    public void productBacklogGetNumberOfEstimates() {
        ProductBacklog productBacklog = new ProductBacklog();

        ProductBacklogItem login = new ProductBacklogItem("Login");
        productBacklog.addItem(login);

        ProductBacklogItem overview = new ProductBacklogItem("Overview");
        productBacklog.addItem(overview);

        ProductBacklogItem search = new ProductBacklogItem("Search");
        productBacklog.addItem(search);

        assertEquals(3, productBacklog.getNumberOfEstimates());
    }


    //* -- REMOVE ME --

    // Maak een methode die door de lijst van PBI's loopt,
    // en een totaal estimate berekent en retourneert

    @Test
    public void productBacklogGetSumOfEstimates() {
        ProductBacklog productBacklog = new ProductBacklog();

        ProductBacklogItem login = new ProductBacklogItem("Login");
        login.setEstimate(7);
        productBacklog.addItem(login);

        ProductBacklogItem overview = new ProductBacklogItem("Overview");
        overview.setEstimate(13);
        productBacklog.addItem(overview);

        ProductBacklogItem search = new ProductBacklogItem("Search"); // Standaard estimate waarde van 1
        productBacklog.addItem(search);

        assertEquals(21, productBacklog.getSumOfEstimates());
    }


    //* -- REMOVE ME --

    // Maak een methode die door de lijst van PBI's loopt,
    // en het item met de hoogste estimate retourneert

    @Test
    public void productBacklogGetHighestEstimate() {
        ProductBacklog productBacklog = new ProductBacklog();

        ProductBacklogItem login = new ProductBacklogItem("Login");
        login.setEstimate(7);
        productBacklog.addItem(login);

        ProductBacklogItem overview = new ProductBacklogItem("Overview");
        overview.setEstimate(13);
        productBacklog.addItem(overview);

        ProductBacklogItem highestItems = productBacklog.getHighestEstimate();

        assertNotNull(highestItems);
        assertEquals(overview, highestItems);
    }


    //* -- REMOVE ME --

    // Maak een nieuwe class UserStory aan die ProductBacklogItem
    // overerft (extend) en voeg aan UserStory een field acceptanceCriteria toe

    @Test
    public void userStoryClass() {
        ProductBacklog productBacklog = new ProductBacklog();

        UserStory logoutStory = new UserStory();
        logoutStory.setSummary("As a user I want to logout");
        logoutStory.setEstimate(7);
        logoutStory.setAcceptanceCriteria("Redirect to login");

        productBacklog.addItem(logoutStory);

        UserStory story = (UserStory) productBacklog.getItem(0);
        assertEquals("Redirect to login", story.getAcceptanceCriteria());
    }


    //* -- REMOVE ME --

    // Exceptions

    // Pas de setEstimate method aan zodat hij bij negatieve waarde een IllegalArgumentException gooit
    // Illegal Argument is een runtime exception, deze hoeven we dus niet expliciet af te vangen

    @Test(expected = Exception.class)
    public void testForIllegalArgumentException() {
        ProductBacklogItem pbiDetail = new ProductBacklogItem("Detail");
        pbiDetail.setEstimate(-1);
    }


    //* -- REMOVE ME --

    // We gaan nu onze eigen exception gebruiken.
    // Hiervoor maken we eerst een eigen class aan die IllegalEstimateException extend

    @Test
    public void illegalEstimateException() {
        IllegalEstimateException illegalEstimateException = new IllegalEstimateException();
        assertNotNull(illegalEstimateException);

        // Tip: IllegalEstimateException extends RuntimeException
        assertTrue(illegalEstimateException instanceof IllegalEstimateException);
    }


    //* -- REMOVE ME --

    // Voeg een constructor toe voor de String message die de super constructor aanroept

    @Test
    public void illegalEstimateExceptionWithMessage() {
        IllegalEstimateException illegalEstimateException = new IllegalEstimateException("This is the error message");
        assertNotNull(illegalEstimateException);

        assertEquals("This is the error message", illegalEstimateException.getMessage());
    }


    //* -- REMOVE ME --

    // Pas de setEstimate method aan zodat hij bij negatieve waarde een IllegalEstimateException gooit

    @Test(expected = IllegalEstimateException.class)
    public void testForIllegalEstimateException() {
        ProductBacklogItem pbiDetail = new ProductBacklogItem("Detail");
        pbiDetail.setEstimate(-1);
    }


    //* -- REMOVE ME --

    // Pas de setEstimate method aan zodat hij bij negatieve waarde een IllegalEstimateException gooit
    // met de melding "Invalid value: -1, estimate value should be 0 or more"

    // Let op: we gebruiken nu in deze test een JUnit Rule om de exceptie te verifieren inplaats van de aanpak met een expected argument voor de Test annotatie

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testForIllegalEstimateExceptionWithNiceMessage() {

        exception.expect(IllegalEstimateException.class);
        exception.expectMessage("Invalid value: -1, estimate value should be 0 or more");

        ProductBacklogItem pbiDetail = new ProductBacklogItem("Detail");
        pbiDetail.setEstimate(-1);
    }


    //* -- REMOVE ME --

    // We gaan nu een verbijzonderde versie van de ProductBacklog class maken die bewaakt dat er niet te veel items op de Backlog komen.
    // Creeer een class LimitedProductBacklog die overerft (extend) van de class ProductBacklog

    @Test
    public void testCreationOfLimitedProductBacklog() {
        LimitedProductBacklog limitedProductBacklog = new LimitedProductBacklog();

        assertNotNull(limitedProductBacklog);
        assertTrue(limitedProductBacklog instanceof ProductBacklog);
        assertTrue(limitedProductBacklog instanceof LimitedProductBacklog);
    }


    //* -- REMOVE ME --

    // Tip : vergeet ook de default constructor niet toe te voegen anders slaagt de test hier boven niet

    @Test
    public void testSettingLimitUsingTheConstructor() {
        LimitedProductBacklog limitedProductBacklog = new LimitedProductBacklog(20);

        assertEquals(20, limitedProductBacklog.getLimit());
    }


    //* -- REMOVE ME --

    // Zorg ervoor dat de default constructor een default waarde van 100 definieert

    @Test
    public void testDefaultLimitUsingTheDefaultConstructor() {
        LimitedProductBacklog limitedProductBacklog = new LimitedProductBacklog();

        assertEquals(100, limitedProductBacklog.getLimit());
    }


    //* -- REMOVE ME --

    // Je hoeft hier niets te doen, deze test toont aan dat jouw LimitedProductBacklog
    // zich hetzelfde gedraagd als de ProductBacklog

    @Test
    public void testShowDelegationToProductBacklog() {
        ProductBacklog productBacklog = new LimitedProductBacklog();

        ProductBacklogItem login = new ProductBacklogItem("Login");
        login.setEstimate(7);
        productBacklog.addItem(login);

        ProductBacklogItem overview = new ProductBacklogItem("Overview");
        overview.setEstimate(13);
        productBacklog.addItem(overview);

        ProductBacklogItem search = new ProductBacklogItem("Search");
        productBacklog.addItem(search);

        assertEquals(21, productBacklog.getSumOfEstimates());
    }


    //* -- REMOVE ME --

    // Implementeer een nieuwe exception BacklogLimitReachedException die overerft (extend) van RuntimeException

    @Test
    public void testImplementationOfBacklogLimitReachedException() {
        BacklogLimitReachedException backlogLimitReachedException = new BacklogLimitReachedException();

        assertNotNull(backlogLimitReachedException);
        assertTrue(backlogLimitReachedException instanceof BacklogLimitReachedException);
        assertTrue(backlogLimitReachedException instanceof RuntimeException);
    }


    //* -- REMOVE ME --

    // Voeg een constructor toe voor de String message die de super constructor aanroept

    @Test
    public void testAddConstructorWithMessageForBacklogLimitReachedException() {

        BacklogLimitReachedException backlogLimitReachedException = new BacklogLimitReachedException("This is the error message");

        assertEquals("This is the error message", backlogLimitReachedException.getMessage());

    }


    //* -- REMOVE ME --

    // Override de addItem functie in de LimitedProductBacklog
    // Zorg dat deze methode de BacklogLimitReachedException gooit als de totale backlog score + de score van het toe te voegen item hoger is dan de limiet

    @Test
    public void testOverridingTheAddMethod() {
        exception.expect(BacklogLimitReachedException.class);
        exception.expectMessage("Could not add item, limit reached total score is: 20");

        ProductBacklog productBacklog = new LimitedProductBacklog(10);

        ProductBacklogItem login = new ProductBacklogItem("Login");
        login.setEstimate(7);
        productBacklog.addItem(login);

        ProductBacklogItem overview = new ProductBacklogItem("Overview");
        overview.setEstimate(13);
        productBacklog.addItem(overview);
    }

}

